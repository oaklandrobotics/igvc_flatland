^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package igvc_flatland
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.1.0 (2022-12-26)
------------------
* Handle new version of OpenCV used in ROS Noetic
* Added BSD license file
* Contributors: Micho Radovnikovich

1.0.1 (2017-12-27)
------------------
* Added install rules
* Contributors: Micho Radovnikovich

1.0.0 (2017-12-27)
------------------
* First release
* Contributors: Micho Radovnikovich
